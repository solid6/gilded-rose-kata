<?php

declare(strict_types=1);

namespace GildedRose\refactor;

final class Item
{
    /**
     * @var string
     */
    public string $name;

    /**
     * @var int
     */
    public int $sell_in;

    /**
     * @var int
     */
    public int $quality;

    public function __construct(string $name, int $sell_in, int $quality)
    {
        $this->name = $name;
        $this->sell_in = $sell_in;
        $this->quality = $quality;
    }

    public function __toString(): string
    {
        return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
}
