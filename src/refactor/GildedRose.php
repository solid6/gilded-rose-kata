<?php

declare(strict_types=1);

namespace GildedRose\refactor;
require 'app_config.php';

final class GildedRose
{
    /**
     * @var Item[]
     */
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    private function decreaseSellIn(Item $item):void
    {
        $item->sell_in --;
    }

    private function decreaseQuality(Item $item,int $amount):void
    {
        if(!($item->quality > PRODUCT_MIN_QUALITY)) return;
        $item->quality -= $amount;
    }

    private function increaseQuality(Item $item,int $amount):void
    {
        if(($item->quality + $amount) > PRODUCT_MAX_QUALITY){
            $item->quality = PRODUCT_MAX_QUALITY;
            return;
        }
        $item->quality += $amount;
    }



    public function updateQuality(): void
    {

        foreach ($this->items as $item) {
            $qualityLoss = $item->sell_in > 0 ? PRE_SALE_QUALITY_LOSS : POST_SALE_QUALITY_LOSS;

            switch ($item->name){
                case PRODUCT_AGED_BRIE:
                    $qualityGain = $item->sell_in > 0 ? PRODUCT_AGED_BRIE_PRE_SALE_QUALITY_GAIN : PRODUCT_AGED_BRIE_POST_SALE_QUALITY_GAIN;
                    $this->increaseQuality($item, $qualityGain);
                    $this->decreaseSellIn($item);
                    break;
                case PRODUCT_BACKSTAGE_TICKETS:
                    if($item->sell_in <= 0){
                        $item->quality = 0;
                        $this->decreaseSellIn($item);
                        break;
                    }
                    if($item->sell_in > PRODUCT_BACKSTAGE_TICKETS_SELL_THRESHOLD_10) $this->increaseQuality($item,PRE_SALE_QUALITY_LOSS);
                    if($item->sell_in <= PRODUCT_BACKSTAGE_TICKETS_SELL_THRESHOLD_10) $this->increaseQuality($item,PRODUCT_BACKSTAGE_TICKETS_SELL_THRESHOLD_10_QUALITY_GAIN);
                    if($item->sell_in <= PRODUCT_BACKSTAGE_TICKETS_SELL_THRESHOLD_5) $this->increaseQuality($item, (PRODUCT_BACKSTAGE_TICKETS_SELL_THRESHOLD_5_QUALITY_GAIN - PRODUCT_BACKSTAGE_TICKETS_SELL_THRESHOLD_10_QUALITY_GAIN));
                    $this->decreaseSellIn($item);
                    break;
                case PRODUCT_CONJURED_MANA_CAKE:
                    $this->decreaseQuality($item,$qualityLoss * 2);
                    $this->decreaseSellIn($item);
                    break;
                case PRODUCT_SULFURAS:
                    break;
                default:
                    $this->decreaseQuality($item, $qualityLoss);
                    $this->decreaseSellIn($item);
            }
        }
    }
}
